import traceback
import time
import argparse
import gspread
import json
from decimal import Decimal

"""
This uses gspread - which needs a GCP project with the relevant permissions set up
https://docs.gspread.org/en/latest/oauth2.html#enable-api-access-for-a-project

The creds in the data directory : 
    data/client_secret_456366820696-nca4eipb876kfuab63l82f4ogd7gur9b.apps.googleusercontent.com.json
    
Point to AdamF-Test-Project

authorised_user.json is tied to your google account - so don't check this into git.

If there is no authorised_user.json, then the app will direct you to a page to OAuth in.  

"""


# The ID and range of a sample spreadsheet.
# Get the spreadsheet ID from the URL of the spreadsheet :
# https://docs.google.com/spreadsheets/d/1mj1HMSuxNd8qCOYiIJxC7GvmV-4l6YU5j0ChQ-jUIIA/edit#gid=1628681872
#
# IMPORTANT - it needs to be a google sheet, not an XLSX or anything else

# Sept 21
# DATA_SPREADSHEET_ID = '1mj1HMSuxNd8qCOYiIJxC7GvmV-4l6YU5j0ChQ-jUIIA'
# Dec 21
DATA_SPREADSHEET_ID = '1MOirvhputLnv18AXmKdeaIM25fLfRTs1rlemx5q5WOs'

WORKSHEET_NAMES = ['Growth P&P',
                   'Growth People',
                   'Growth Planet',
                   'Balanced P&P',
                   'Balanced People',
                   'Balanced Planet',
                   'Cautious P&P',
                   'Cautious People',
                   'Cautious Planet']

MANDATE_LOCATION = (1, 0)

CARBON_DATA_LOCATION = (15, 0)
CARBON_DESC_LOCATION = (15, 1)

POSITIVE_DATA_LOCATION = (18, 0)
POSITIVE_DESC_LOCATION = (18, 1)

TOP_OF_ASSETS_TABLE = 22
HOLDING_NAME = 0
HOLDING_WEIGHT = 1
HOLDING_DESC = 2

START = 0
END = 1


class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, Decimal):
            return float(o)
        return super(DecimalEncoder, self).default(o)


def row_to_asset(row: [], asset_descriptions: dict) -> dict:
    """
    Process a row into an asset
    If the asset has a description - update the asset_descriptions - otherwise use the description from asset_descriptions if there is one.
    :param row:
    :param asset_descriptions:
    :return:
                    {
                        'name' : ''
                        'marketValue' : 99.99
                        'weight' : 99.99
                        'description' : 'a longer descriptive string'
                    }

    """

    NAME = 0
    WEIGHT = 1
    DESCRIPTION = 2

    retval = {'name': row[NAME],
              'weight': Decimal(row[WEIGHT])}

    if row[DESCRIPTION] != '':
        retval['description'] = row[DESCRIPTION]
        asset_descriptions[row[NAME]] = row[DESCRIPTION]
    elif row[NAME] in asset_descriptions:
        retval['description'] = asset_descriptions[row[NAME]]

    return retval


def process_asset_groups(sheet: [[]], asset_descriptions: dict, asset_group_name: str, asset_group_bounds: []) -> dict:
    # process equities
    ag_dict = {
        'assetGroupType': asset_group_name,
        'weight': Decimal(sheet[asset_group_bounds[START] - 1][1]),
        'assets': []
    }

    for i in range(asset_group_bounds[START], asset_group_bounds[END] + 1):
        ag_dict['assets'].append(row_to_asset(sheet[i], asset_descriptions))

    return ag_dict


def sheet_to_asset_dict(sheet: [[]], asset_descriptions: dict) -> dict:
    """
    Dict format:

    {
        'assetGroups' : [
            {
                'assetGroupType' : ''
                'marketValue' : 99.99
                'weight' : 99.99
                'assets' : [
                    {
                        'name' : ''
                        'marketValue' : 99.99
                        'weight' : 99.99
                        'description' : 'a longer descriptive string'
                    }
                ]
            }
        ]
    }

    """

    # first scan through to find the various blocks
    equities = [-1, -1]
    bonds = [-1, -1]
    others = [-1, -1]
    cash = -1

    # this is a very naive scan which assumes that asset categories will always appear in the same order, and that there
    # are no gaps in the table
    for i in range(TOP_OF_ASSETS_TABLE, len(sheet)):
        if sheet[i][0].strip() == 'Total Equities':
            equities[START] = i+1
        if sheet[i][0].strip() == 'Total Bonds':
            equities[END] = i-1
            bonds[START] = i+1
        if sheet[i][0].strip() == 'Total Other':
            bonds[END] = i-1
            others[START] = i+1
        if sheet[i][0].strip() == 'Cash':
            others[END] = i-1
            cash = i

    # Create the response
    retval = {'assetGroups': []}

    # process equities
    equities_dict = process_asset_groups(sheet, asset_descriptions, 'Equities', equities)
    retval['assetGroups'].append(equities_dict)

    # process bonds
    bonds_dict = process_asset_groups(sheet, asset_descriptions, 'Bonds', bonds)
    retval['assetGroups'].append(bonds_dict)

    # process others
    others_dict = process_asset_groups(sheet, asset_descriptions, 'Other', others)
    retval['assetGroups'].append(others_dict)

    #cash
    cash_dict = {
        'assetGroupType': 'Cash',
        'weight': Decimal(sheet[cash][1]),
    }
    retval['assetGroups'].append(cash_dict)

    return retval

'''
Dict format: 

{
    'mandate' : 'a text description of the mandate'
    'carbonFootprint' :
    {
        'value' : 99.99
        'description' : '% less than MCSI world'
    }
    'positiveImpact' : 
    {
        'value' : 99.99
        'description' : '% directly contributing to UN SDGs'
    }
}
'''


def sheet_to_data_dict(sheet: [[]]) -> dict:

    mandate = sheet[MANDATE_LOCATION[0]][MANDATE_LOCATION[1]]
    carbon_value = sheet[CARBON_DATA_LOCATION[0]][CARBON_DATA_LOCATION[1]]
    carbon_desc = sheet[CARBON_DESC_LOCATION[0]][CARBON_DESC_LOCATION[1]]
    positive_data = sheet[POSITIVE_DATA_LOCATION[0]][POSITIVE_DATA_LOCATION[1]]
    positive_desc = sheet[POSITIVE_DESC_LOCATION[0]][POSITIVE_DESC_LOCATION[1]]

    return {
            'mandate': mandate,
            'carbonFootprint': {
                'value': carbon_value,
                'description': carbon_desc
                },
            'positiveImpact': {
                'value': positive_data,
                'description': positive_desc
                }
            }


def main():
    gc = gspread.oauth(credentials_filename='data/client_secret_456366820696-nca4eipb876kfuab63l82f4ogd7gur9b.apps.googleusercontent.com.json', authorized_user_filename='data/authorised_user.json')
    sheet = gc.open_by_key(DATA_SPREADSHEET_ID)

    asset_descriptions = {}

    for worksheet in sheet.worksheets():
        if worksheet.title.strip() in WORKSHEET_NAMES:
            print(f'Processing {worksheet.title}')
            sheet_data = worksheet.get_all_values()
            d = sheet_to_data_dict(sheet_data)
            with open(f'outputs/{worksheet.title} other data.json', 'wt') as outfile:
                json.dump(d, outfile, cls=DecimalEncoder, indent=2)

            e = sheet_to_asset_dict(sheet_data, asset_descriptions)
            with open(f'outputs/{worksheet.title} asset allocation.json', 'wt') as outfile:
                json.dump(e, outfile, cls=DecimalEncoder, indent=2)
        else:
            print(f'Skipped {worksheet.title}')


if __name__ == '__main__':
    try:
        start_time = time.time()
        parser = argparse.ArgumentParser()
        parser.add_argument('-v', '--verbose', action='store_true', default=False, help='verbose output')
        args = parser.parse_args()
        if args.verbose:
            print(time.asctime())

        main()

        if args.verbose:
            print(time.asctime())
            print('TOTAL TIME IN MINUTES:')
            print((time.time() - start_time) / 60.0)

    except KeyboardInterrupt as e:  # Ctrl-C
        raise e
    except SystemExit as e:  # sys.exit()
        raise e
    except Exception as e:
        print('ERROR, UNEXPECTED EXCEPTION')
        print(str(e))
        traceback.print_exc()
